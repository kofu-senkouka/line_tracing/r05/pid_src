#define DELTA_T 0.010//演算周期。演算周期で割ることで1秒にしている
// #define KP 0.165
#define KP 0.40 //限界値感度法から微調整
// #define KP 0.61 //
// #define KP 0.00 //
// #define KI 0.013
#define KI 0.025 //限界値感度法50ms
// #define KI 0.035
// #define KI 0.05
// #define KD 0.0007
#define KD 0.00625 //限界値感度法50ms
// #define KD 0.0035 //
// #define KD 0.000033 //
// #define KD 0.0000 //


//////////////////PID値の制限//////////////////
float math_limit(float pid) {
  pid = constrain(pid, -1023 , 1023); //定義域-255 <= pid <= 255
  return pid;
}

//////////////////左車輪のPID制御//////////////////
float PID_LIGHT_L(short Senser_Value, short Target_Value){

   static long diff_L[2]={0};
   static float integral_L=0;
   float p=0,i=0,d=0;

   diff_L[0] = diff_L[1]; //前回の偏差の値を格納
   diff_L[1] = Target_Value - Senser_Value; //ターゲットとの差を格納
   integral_L += (diff_L[1] + diff_L[0]) * DELTA_T / 2.0; //積分はグラフの面積(台形)

   p = KP * diff_L[1];
   i = KI * integral_L;
   d = KD * (diff_L[1] - diff_L[0])/DELTA_T ; //秒数がmsなので"偏差の差"で十分

   return math_limit(p+i+d);
}

//////////////////右車輪のPID制御//////////////////
float PID_LIGHT_R(short Senser_Value, short Target_Value){

   static long diff_R[2]={0};
   static float integral_R=0;
   
   float p=0,i=0,d=0;

   diff_R[0] = diff_R[1];
   diff_R[1] = Target_Value - Senser_Value;
   integral_R += ( diff_R[1] + diff_R[0]) * DELTA_T / 2.0; 

   p = KP * diff_R[1] ;
   i = KI * integral_R ;
   d = KD * (diff_R[1] - diff_R[0])/DELTA_T ; 

   return math_limit(p+i+d);
}
