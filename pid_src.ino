#include <LCD_I2C.h>
#include "moter.h"

#define TEST_SW1 (20)//テストSW1はGP20
#define TEST_SW2 (21)//テストSW2はGP21
#define TEST_LED1 (12)//テストLED1はGP12
#define TEST_LED2 (13)//テストLED2はGP13

struct repeating_timer st_timer; //タイマー割り込み用の構造体
LCD_I2C lcd(0x27, 16, 2);//LCD関数をインスタンス化
char lcd_buff[2][17];//16文字＋エンドコードで17文字分確保([0]=1行目,[1]=2行目)

//  タイマー割り込み処理
int sen2ad=0,sen3ad=0;
float pwm_r=0, pwm_l=0, Pid_val_R=0, Pid_val_L=0;
bool Timer(struct repeating_timer *t) {

  sen2ad = sen2_ad(); //センサ2AD変換
  sen3ad = sen3_ad(); //センサ3AD変換

  Pid_val_L = PID_LIGHT_L(sen2ad, 750); //左モータのPID制御量を演算
  Pid_val_R = PID_LIGHT_R(sen3ad, 750); //右モータのPID制御量を演算
  pwm_l = constrain( (700 + Pid_val_L - Pid_val_R), 0, 1023 ); //左モータのPWM値を求める
  pwm_r = constrain( (700 + Pid_val_R - Pid_val_L), 0, 1023 ); //右モータのPWM値を求める
  moter_pwm_set(L, pwm_l); //左モータのPWM設定
  moter_pwm_set(R, pwm_r); //右モータのPWM設定

  //LCDに表示する内容
  snprintf(lcd_buff[0], 16, "%04d, %04d", sen2ad, sen3ad); //LCD1行目にAD変換の値を表示
  //動作確認用にテストLEDをフリッカさせる
  if( digitalRead(TEST_LED1) == HIGH ){ digitalWrite(TEST_LED1, LOW); }
  else{ digitalWrite(TEST_LED1, HIGH); }
  return true;
}


void setup() {
  // put your setup code here, to run once:
  main_wait_ini(50);//main処理の待ち時間50msにセット
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TEST_SW1, INPUT_PULLUP);//TEST_SW1をプルアップで入力モードに設定
  pinMode(TEST_SW2, INPUT_PULLUP);//TEST_SW2をプルアップで入力モードに設定
  pinMode(TEST_LED1, OUTPUT);//TEST_LED1を出力モードに設定
  pinMode(TEST_LED2, OUTPUT);//TEST_LED2を出力モードに設定
  lcd.begin();//LCD初期化
  lcd.backlight();//バックライトON
  moter_begin();//モータ関連初期化
  sensor_begin();//センサ関連初期化
  // タイマーの初期化(割込み間隔はusで指定)
  add_repeating_timer_us(10000, Timer, NULL, &st_timer);
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long main_wait_time;//メイン周期の待った時間（us）
  
  main_wait_time = main_wait();//メイン周期待ち関数
  /* この関数以降は50ms周期でループする */
  main_50ms(); //50ms毎の処理
  lcd.setCursor(0, 0);//カーソル位置を1文字目の1行目にする
  lcd.print(lcd_buff[0]);//1行目の文字データを表示
  lcd.setCursor(0, 1);//カーソル位置を1文字目の2行目にする
  lcd.print(lcd_buff[1]);//2行目の文字データを表示

}

// メイン50ms毎処理 /////////////////////////////////////////////////////////////////////
void main_50ms(void){


}
